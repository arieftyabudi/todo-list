import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

// Pinia
import { createPinia } from 'pinia'

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import '@mdi/font/css/materialdesignicons.css'
import {aliases,mdi} from 'vuetify/lib/iconsets/mdi'


const vuetify = createVuetify({
    icons: {
        defaultSet: "mdi",
        aliases,
        sets: {
          mdi
        },
      },
  components,
  directives,
})

const toDOApp = createApp(App)

toDOApp.use(vuetify)
toDOApp.use(createPinia())
toDOApp.mount('#app')
