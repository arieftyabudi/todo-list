import { defineStore } from 'pinia'
import { ref,computed } from 'vue'
import ITodo from '@/type/ITodo'

export const useTodo = defineStore('todo', ()=>{

    const data =ref<ITodo[]>([])
    const deletedData=ref<ITodo[]>([])
    const dataFilter = ref<string>("All")
   
    const completedTasks =computed<number>(()=> data.value.filter(task => task.done).length) 
 
    const progress =computed<number>(()=> completedTasks.value / data.value.length * 100) 
      
    const remainingTasks=computed<number>(()=>data.value.length - completedTasks.value)  
    
    return { data,completedTasks,progress ,remainingTasks,deletedData,dataFilter}
  })