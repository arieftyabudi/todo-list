export default interface ITodo{
    id:number
    done:boolean
    text:string
}